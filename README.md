# BitBucket CLI

Create and manage repositories from the command line.

## Quick Start

Coming soon...

## Motivation

I love BitBucket for many reasons, but having to jump into any web-based GUI when starting a new project is a pain. So I created this script to help me setup remote repositories quickly from the command line.

This script has an intentionally sparse feature set. I created it solely to manage repositories, and that is what it does. If you'd like to add any additional functionality, please feel free to take out a pull request. I love contributions, but as the current app stands it is unlikely that I will be adding new features. I will of course respond to new issues and bug reports.

This script was also inspired by [zhemao's BitBucket CLI written in Python][bitpy]. For anyone looking for more advanced functionality I would highly recommend the check out that project.

## Changelog

- **0.1.0**

Create project.
